#ifndef BREAKPOINT_H
#define BREAKPOINT_H

typedef struct {
    size_t    count;
    size_t    max;
    uint64_t* points;
} BreakpointList;

#endif
